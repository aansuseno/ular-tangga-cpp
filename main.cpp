#include <iostream>

// ganti #include <windows.h> untuk pengguna Windows
#include <unistd.h> // untuk sleep

#include <cstdlib> // juntuk flush
#include <ctime>
using namespace std;

int posisi[2] = {0,0};

// silahkan ubah sesuai keinginan anda
int ular[7][2] = {{36,6}, {32,10}, {48,26}, {63,18}, {88,24}, {95,56}, {97,78}};
int tangga[7][2] = {{1,38}, {4,14}, {8,10}, {28,76}, {21,42}, {50,67}, {71,92}};

// untuk membersihkan layar
void clear() {
	cout<<"\033[2J\033[1;1H";
}

void jeda(int dtk) {
	// ganti Sleep(dtk*1000); untuk pengguna Windows
	sleep(dtk);
}

int dadu() {
	srand((unsigned) time(0));
	int angka = (rand()%6)+1;
	return angka;
}

void mulai();

void KompJalan() {
	clear();
	cout<<"Anda :"<<posisi[0]<<endl;
	cout<<"Komp :"<<posisi[1]<<endl;
	cout<<"Komp melempar dadu";
	cout.flush();
	jeda(1);
	cout<<".";
	cout.flush();
	jeda(1);
	cout<<".";
	cout.flush();
	jeda(1);
	cout<<".\n";

	int angkaAcak = dadu();
	cout<<"Komp mendapat angka "<<angkaAcak;
	cout<<"\nKomp jalan \n";
	for (int i = 0; i < angkaAcak; ++i) {
		posisi[1]++;
		cout<<posisi[1]<<endl;
		cout.flush();
		jeda(1);
	}

	int banyakUlar = sizeof(::ular)/sizeof(*::ular);
	for (int i = 0; i < banyakUlar; ++i) {
		if (posisi[1] == ::ular[i][0]) {
			posisi[1] = ::ular[i][1];
			cout<<"Horay.. Komp menginjak ular. Posisi Komp menjadi "<<posisi[1]<<endl;
			cout.flush();
			jeda(3);
		}
	}

	int banyakTangga = sizeof(::tangga)/sizeof(*::tangga);
	for (int i = 0; i < banyakTangga; ++i) {
		if (posisi[1] == ::tangga[i][0]) {
			posisi[1] = ::tangga[i][1];
			cout<<"Uh Oh.. Komp mendapat tangga. Posisi Komp menjadi "<<posisi[1]<<endl;
			cout.flush();
			jeda(3);
		}
	}

	if (posisi[1] >= 100) {
		cout<<"Komp menang\n";
	} else {
		if (angkaAcak == 6) {
			cout<<"Komp mendapat angka 6\nJadi sekarang giliran Komp lagi\n";
			cout.flush();
			jeda(2);
			KompJalan();
		} else {
			mulai();
		}
	}
}

void kocok() {
	clear();
	cout<<"Anda :"<<posisi[0]<<endl;
	cout<<"Komp :"<<posisi[1]<<endl;
	cout<<"Anda melempar dadu";
	cout.flush();
	jeda(1);
	cout<<".";
	cout.flush();
	jeda(1);
	cout<<".";
	cout.flush();
	jeda(1);
	cout<<".\n";

	int angkaAcak = dadu();
	cout<<"Anda mendapat angka "<<angkaAcak;
	cout<<"\nAnda jalan \n";
	for (int i = 0; i < angkaAcak; ++i) {
		posisi[0]++;
		cout<<posisi[0]<<endl;
		cout.flush();
		jeda(1);
	}

	int banyakUlar = sizeof(ular)/sizeof(*ular);
	for (int i = 0; i < banyakUlar; ++i) {
		if (posisi[0] == ular[i][0]) {
			posisi[0] = ular[i][1];
			cout<<"Uh oh.. Anda menginjak ular. Posisi anda menjadi "<<posisi[0]<<endl;
			cout.flush();
			jeda(3);
		}
	}

	int banyakTangga = sizeof(tangga)/sizeof(*tangga);
	for (int i = 0; i < banyakTangga; ++i) {
		if (posisi[0] == tangga[i][0]) {
			posisi[0] = tangga[i][1];
			cout<<"Horay.. Anda mendapat tangga. Posisi anda menjadi "<<posisi[0]<<endl;
			cout.flush();
			jeda(3);
		}
	}

	if (posisi[0] >= 100) {
		cout<<"Anda menang\n";
	} else {
		if (angkaAcak == 6) {
			cout<<"Anda mendapat angka 6\nJadi sekarang giliran Anda lagi\n";
			cout<<"Ketik 'kocok' untuk mengocok dadu\n>";
			string masukkan;
			cin>>masukkan;

			if (masukkan == "kocok") {
				kocok();
			} else {
				clear();
				cout<<"Input tidak valid!\n";
				cout.flush();
				jeda(2);
				mulai();
			}
		} else {
			cout<<"Giliran Komp jalan\n";
			cout.flush();
			jeda(1);
			KompJalan();
		}
	}
}

void mulai() {
	clear();
	cout<<"Anda :"<<posisi[0]<<endl;
	cout<<"Komp :"<<posisi[1]<<endl;
	cout<<"Giliran Anda\n";
	cout<<"Ketik 'kocok' untuk mengocok dadu\n>";
	string masukkan;
	cin>>masukkan;

	if (masukkan == "kocok") {
		kocok();
	} else {
		clear();
		cout<<"Input tidak valid!\n";
		cout.flush();
		jeda(2);
		mulai();
	}
}

void sebelumMulai() {
	cout<<"Ketik 'mulai' untuk bermain\n>";
	string masukkan;
	cin>>masukkan;

	if (masukkan == "mulai") {
		clear();
		mulai();
	} else {
		clear();
		cout<<"Input tidak valid!\n";
		sebelumMulai();
	}
}

// untuk menampilkan selamat datang
// setelah aplikasi dijalankan
void welcome() {
	cout<<"SELAMAT DATANG DI GAME\nULAR TANGGA\nBERBASIS TEKS\nby Aan Suseno\n";
	cout<<"----------------------\n";
	sebelumMulai();
}

int main() {
	clear();
	welcome();
}